<?php
/**
 * Created by PhpStorm.
 * User: parallel
 * Date: 8/15/18
 * Time: 5:44 PM
 */

namespace App\Model;


class Character
{
    public $name;
    public $appearsIn;

    /**
     * Character constructor.
     * @param string $name
     * @param int[] $appearsIn
     */
    public function __construct(string $name, array $appearsIn)
    {
        $this->name = $name;
        $this->appearsIn = $appearsIn;
    }
}