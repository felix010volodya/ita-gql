<?php
/**
 * Created by PhpStorm.
 * User: parallel
 * Date: 8/15/18
 * Time: 5:43 PM
 */

namespace App\Model;


class Repository
{
    private $characters;
    private $starships;

    public function __construct()
    {
        $this->characters = [
            new Character("First character", [1, 2, 3]),
            new Character("Second character", [3]),
        ];

        $this->starships = [
            new Starship(1, "First starship", 159, [3]),
            new Starship(2, "Second starship", 951, [1]),
        ];
    }

    public function getCharacters(int $episode) {
        if($episode) {
            return array_filter($this->characters, function (Character $element) use ($episode) {
                return in_array($episode, $element->appearsIn);
            });
        }
        return $this->characters;
    }

    public function getStarships(int $episode, int $unit) {
        if($episode) {
            /** @var Starship[] $result */
            $result = array_filter($this->starships, function(Starship $element) use ($episode) {
                return in_array($episode, $element->appearsIn);
            });
        } else {
            $result = $this->starships;
        }

        if($unit === 2) {
            foreach ($result as $item) {
                $item->length *= 1609.34;
            }
        }

        return $result;
    }
}