<?php
/**
 * Created by PhpStorm.
 * User: parallel
 * Date: 8/15/18
 * Time: 5:44 PM
 */

namespace App\Model;


class Starship
{
    public $id;
    public $name;
    public $length;
    public $appearsIn;

    /**
     * Starship constructor.
     * @param int $id
     * @param string $name
     * @param float $length
     * @param int[] $appearsIn
     */
    public function __construct(
        int $id,
        string $name,
        float $length,
        array $appearsIn
    )
    {
        $this->name = $name;
        $this->id = $id;
        $this->length = $length;
        $this->appearsIn = $appearsIn;
    }
}