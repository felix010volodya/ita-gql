<?php

namespace App\Resolver;

use App\Model\Repository;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Resolver\ResolverMap as OverblogResolverMap;
use Overblog\GraphQLBundle\Resolver\UnresolvableException;
use GraphQL\Utils;

class ResolverMap extends OverblogResolverMap
{
    /**
     * @var Repository
     */
    private $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    protected function map()
    {
        return [
            'RootQuery' => [
                self::RESOLVE_FIELD => function ($value, Argument $args, \ArrayObject $context, ResolveInfo $info) {
                    if ('characters' === $info->fieldName) {
                        return $this->repository->getCharacters((int) $args['episode']);
                    }
                    if ('starships' === $info->fieldName) {
                        return $this->repository->getStarships((int) $args['episode'], (int) $args['unit']);
                    }
                    return null;
                },
            ],

            'Episode' => [
                'NEWHOPE' => 1,
                'EMPIRE' => 2,
                'JEDI' => 3,
            ],

            'LengthUnit' => [
                'METER' => 1,
                'MILE' => 2,
            ],
        ];
    }
}